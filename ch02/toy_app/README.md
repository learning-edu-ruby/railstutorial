# Ruby on Rails Tutorial

## "toy app"

This is the first application for the
[*Ruby on Rails Tutorial*](https://www.railstutorial.org/)
by [Michael Hartl](https://www.michaelhartl.com/). Hello, world!

## Installation

Project is built with Ruby version `2.6.0` and runs on Rails.

In order to run the application make sure that Ruby version `2.6.0` is installed. It is better to use ruby version manager like RVM, rbenv or similar.

To install needed Ruby version for rbenv execute following command

    rbenv install 2.6.0

After this rails should be installed with the following command:

    gem install rails

Then project gems must be installed. Change to this project directory and run following command:

    bundle install --without production

## Running on local machine

To run application on local machine execute following command

    bin/rails s -e development
