# Ruby on Rails Tutorial

## "hello, world!"

This is the first application for the
[*Ruby on Rails Tutorial*](https://www.railstutorial.org/)
by [Michael Hartl](https://www.michaelhartl.com/). Hello, world!

## Installation

Project is built with Ruby version `2.5.1` and runs on Rails version `5.1.6`.

In order to run the application make sure that Ruby version `2.5.1` is installed. It is better to use ruby version manager like RVM, rbenv or similar.

To install needed Ruby version for rbenv execute following command

    rbenv install 2.5.1

After this rails version `5.1.6` should be installed with the following command:

    gem install rails -v 5.1.6

Then project gems must be installed. Change to this project directory and run following command:

    bundle install --without production

## Running on local machine

To run application on local machine execute following command

    bin/rails s -e development
