# 4.2.1 Comments
puts 17 + 42 # Integer addition
puts

# 4.2.2 Strings
puts ''
puts 'foo'

puts 'foo' + 'bar'

first_name = 'Edu'
puts first_name
puts "#{first_name} Finn"

last_name = 'Finn'
puts first_name
puts last_name
puts first_name + ' ' + last_name
puts "#{first_name} #{last_name}"
puts

# Printing
puts 'foo'
print 'foo'
print "foo\n"
puts

# Single-quoted strings
puts 'foo'
puts "foo"
puts 'foo' + 'bar'
puts "foo" + "bar"
foo = 'foo'
puts '#{foo} bar'
puts "#{foo} bar"
puts '\n'
puts 'Newlines (\n) and tabs (\t) both use the backslash character \.'
puts

# Exercises
city = 'Stäfa'
canton = 'ZH'
puts "#{city}, #{canton}"
puts "#{city}\t#{canton}"
puts '#{city}\t#{canton}'
puts

# 4.2.3 Objects and message passing
puts 'foobar'.empty?
puts ''.empty?

s = 'foobar'
if s.empty?
    puts 'The string is empty'
else
    puts 'The string is nonempty'
end

if s.nil?
    puts 'The string is nil'
elsif s.empty?
    puts 'The string is empty'
elsif s.include?('foo')
    puts 'The string includes "foo"'
end

x = 'foo'
y = ''
puts 'Both strings are empty' if x.empty? && y.empty?
puts 'One of the strings are empty' if x.empty? || y.empty?
puts 'x is not empty' if !x.empty?
puts

p nil.to_s
# nil.empty? # NoMethodError
puts nil.to_s.empty?
puts 'foo'.nil?
puts ''.nil?
puts nil.nil?
puts

string = 'foobar'
puts "The string '#{string}' is nonempty." unless string.empty?

puts !!nil
puts !!0
puts

# Exercises
puts 'racecar'.length

puts 'racecar'.reverse

s = 'racecar'
puts s == s.reverse

puts "It's a palindrome!" if s == s.reverse
puts

# 4.2.4 Method definitions
def string_message(str = '')
  if str.empty?
    "It's an empty string!"
  else
    'The string is nonempty.'
  end
end
puts string_message('foobar')
puts string_message('')
puts string_message

def string_message2(str = '')
  return 'It is an empty string!' if str.empty?
  return 'The string is nonempty.'
end
puts string_message2('foobar')
puts string_message2('')
puts string_message2

def string_message3(str = '')
  return 'It is an empty string!' if str.empty?
  'The string is nonempty.'
end
puts string_message3('foobar')
puts string_message3('')
puts string_message3
puts

# Exercises
def palindrome?(s)
  s == s.reverse
end

def palindrome_tester(s)
  if palindrome?(s)
    puts 'It is a palindrome!'
  else
    puts 'It is not a palindrome.'
  end
end
palindrome_tester('racecar')
palindrome_tester('onomatopoeia')

puts palindrome_tester('racecar').nil?
