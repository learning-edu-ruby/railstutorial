# 4.3.1 Arrays and ranges
p 'foo bar    baz'.split
p 'fooxbarxbaz'.split('x')
puts

a = [42, 8, 17]
p a
puts a [0]
puts a [1]
puts a [2]
puts a [-1]

puts a.first
# puts a.second
puts a.last
puts a.last == a[-1]
puts

x = a.length
puts x
puts x == 3
puts x == 1
puts x != 1
puts x > 1
puts x < 1
puts

p a
puts a.empty?
puts a.include?(42)
p a.sort
p a.reverse
p a.shuffle
p a
puts

p a
p a.sort!
p a
puts

p a.push(6)
p a << 7
p a << 'foo' << 'bar'

puts a.join
puts a.join(', ')
puts

# ranges
p 0..9
# 0..9.to_a # call to_a on 9
p (0..9).to_a

a = %w[foo, bar, baz, quux]
p a
p a[0..2]

a = (0..9).to_a
p a
p a[2..(a.length - 1)]
p a[2..-1]

p ('a'..'e').to_a
puts

# Exercises
a = 'A man, a plan, a canal, Panama'.split(', ')
p a

s = a.join
puts s

s = s.split.join
puts s

def palindrome?(s)
  s == s.reverse
end
def palindrome_tester(s)
  if palindrome?(s)
    puts 'It is a palindrome!'
  else
    puts 'It is not a palindrome.'
  end
end
puts palindrome_tester(s)
puts palindrome_tester(s.downcase)

puts ('a'..'z').to_a[7]
puts ('a'..'z').to_a[-7]

# 4.3.2 Blocks
(1..5).each { |i| puts 2 * i }

(1..5).each do |number|
  puts 2 * number
  puts '--'
end

3.times { puts 'Betelgeuse!' }

p (1..5).map { |i| i**2 }
p %w[a b c]
p %w[a b c].map { |char| char.upcase }
p %w[A B C].map { |char| char.downcase }
p %w[A B C].map(&:downcase)
puts

p ('a'..'z').to_a
p ('a'..'z').to_a.shuffle
p ('a'..'z').to_a.shuffle[0..7]
p ('a'..'z').to_a.shuffle[0..7].join
puts

# Exercises
p (0..16).to_a.map { |i| i**2 }

def yeller(a)
  a.map(&:upcase).join
end
puts yeller(['o', 'l', 'd'])

def random_subdomain
  ('a'..'z').to_a.shuffle[0..7].join
end
puts random_subdomain

def string_shuffle(s)
  s.split('').shuffle.join
end
puts string_shuffle('foobar')
puts

# 4.3.3 Hashes and symbols
user = {}
p user
user['first_name'] = 'Edu'
user['last_name'] = 'Finn'
p user['first_name']
p user

user = { 'first_name' => 'Edu', 'last_name' => 'Finn' }
p user
puts

p 'name'.split('')
# :name.split('') # symbols doesn't have strings functions
'foobar'.reverse
# :foobar.reverse

# cannot use numbers and special characters in symbol names
# :foo-bar
# :2foo
puts

user = { :name => 'Edu Finn', :email => 'edu@example.com' }
p user
p user[:name]
p user[:password]

h1 = { :name => 'Edu Finn', :email => 'edu@example.com' }
h2 = { name: 'Edu Finn', email: 'edu@example.com' }
p h1
p h2
p h1 == h2
puts

params = {}
p params
params[:user] = { name: 'Edu Finn', email: 'edu@example.com' }
p params
p params[:user][:email]
puts

flash = { success: 'It worked!', danger: 'It failed.' }
p flash
flash.each do |key, value|
  puts "Key #{key.inspect} has value #{value.inspect}"
end
puts

puts (1..5).to_a
puts (1..5).to_a.inspect

puts :name, :name.inspect
puts 'It worked!', 'It worked!'.inspect

p :name
puts

# Exercises
h = { one: 'uno', two: 'dos', three: 'tres' }
h.each { |k, v| puts "'#{k}' in Spanish is '#{v}'" }

person1 = { first: 'Eino' }
person2 = { first: 'Kat' }
person3 = { first: 'Edu' }
params = { father: person1, mother: person2, child: person3 }
p params[:father][:first]
p params[:mother][:first]
p params[:child][:first]

h = { name: 'Edu Finn', email: 'edu@exampe.com', password_digest: ('a'..'z').to_a.shuffle[0..15].join }
p h
p h[:password_digest].length
puts
