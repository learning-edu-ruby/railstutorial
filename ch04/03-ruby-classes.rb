# 4.4 Ruby classes
# 4.4.1 Constructors
s = 'foobar'
p s.class

s = String.new('foobar')
p s
p s.class
p s == 'foobar'
puts

a = Array.new([1, 2, 3])
p a
puts

h = Hash.new
p h
p h[:foo]
h = Hash.new(0)
p h
p h[:foo]
puts

# Exercises
p 0..5
p Range.new(0, 5)

p (0..5) == Range.new(0, 5)

# 4.4.2 Class inheritance
s = String.new('foobar')
p s
p s.class
p s.class.superclass
p s.class.superclass.superclass
p s.class.superclass.superclass.superclass
puts

class Word
  def palindrome?(string)
    string == string.reverse
  end
end
w = Word.new
p w
p w.palindrome?('foobar')
p w.palindrome?('level')
puts

class Word2 < String
  def palindrome?
    self == self.reverse
  end
end
s = Word2.new('level')
p s
p s.palindrome?
p s.length

p s.class
p s.class.superclass
p s.class.superclass.superclass
puts

# Exercises
p (0..5).class
p (0..5).class.superclass
p (0..5).class.superclass.superclass
p (0..5).class.superclass.superclass.superclass
puts

p Hash.new().class
p Hash.new().class.superclass
p Hash.new().class.superclass.superclass
p Hash.new().class.superclass.superclass.superclass
puts

p :name.class
p :name.class.superclass
p :name.class.superclass.superclass
p :name.class.superclass.superclass.superclass
puts

class Word3 < String
  def palindrome?
    self == reverse
  end
end
s = Word3.new('level')
p s
p s.palindrome?
puts

# 4.4.3 Modifying built-in classes
class String
  def palindrome?
    self == self.reverse
  end
end
p 'deified'.palindrome?
puts

# Exercises
p 'racecar'.palindrome?
p 'onomatopoeia'.palindrome?
p 'Malayalam'.palindrome?
p 'Malayalam'.downcase.palindrome?

class String
  def shuffle
    self.split('').shuffle.join
  end
end
p 'foobar'.shuffle

class String
  def shuffle
    split('').shuffle.join
  end
end
p 'foobar'.shuffle
